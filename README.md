## Assigment 1 - Image Enhancencement and Filtering
Image Processing - SCC0251

Authors:
* Ana Costa
* Ana Laura Chioca

Folders and files:
* [Python code](ImageFiltering.py) contains the source code submitted
* [Notebook with demo](dip02-filtering.ipynb) is a notebook exemplifying functions developed and submitted
